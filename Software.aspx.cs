﻿using System;
using System.Configuration;
using System.Data.OleDb;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
///     Code behind for Software page
/// </summary>
/// <version>
///     Spring 2015
/// </version>
/// <author>
///     Danl Doremus
/// </author>
public partial class Software : Page
{
    /// <summary>
    ///     The _software object
    /// </summary>
    private SoftwareObject _softwareObject;

    /// <summary>
    ///     Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblError.Text = "";
    }

    /// <summary>
    ///     Handles the Click event of the btnAddSoftware control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnAddSoftware_Click(object sender, EventArgs e)
    {
        this._softwareObject = new SoftwareObject
        {
            SoftwareId = this.txtNewSoftwareId.Text,
            Name = this.txtNewSoftwareName.Text,
            Version = Convert.ToDouble(this.txtNewSoftwareVersion.Text),
            ReleaseDate = Convert.ToDateTime(this.txtNewSoftwareReleaseDate.Text)
        };

        this.InsertSoftware(this._softwareObject);

        this.txtNewSoftwareId.Text = "";
        this.txtNewSoftwareName.Text = "";
        this.txtNewSoftwareVersion.Text = "";
        this.txtNewSoftwareReleaseDate.Text = "";
    }

    /// <summary>
    ///     Inserts the software.
    /// </summary>
    /// <param name="softwareObject">The software object.</param>
    private void InsertSoftware(SoftwareObject softwareObject)
    {
        var connection =
            new OleDbConnection(
                ConfigurationManager.ConnectionStrings["DigitalManagerConnectionString"].ConnectionString);
        const string statement = "INSERT INTO Software "
                                 + " (SoftwareID, Name, Version, ReleaseDate) "
                                 + " VALUES(@SoftwareID, @Name, @Version, @ReleaseDate)";
        var command = new OleDbCommand(statement, connection);
        command.Parameters.AddWithValue("SoftwareID", softwareObject.SoftwareId);
        command.Parameters.AddWithValue("Name", softwareObject.Name);
        command.Parameters.AddWithValue("Version", softwareObject.Version);
        command.Parameters.AddWithValue("ReleaseDate", softwareObject.ReleaseDate);
        connection.Open();
        command.ExecuteNonQuery();
        connection.Close();

        this.gvSoftware.DataBind();
    }

    /// <summary>
    ///     Handles the RowUpdated event of the gvSoftware control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewUpdatedEventArgs" /> instance containing the event data.</param>
    protected void gvSoftware_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A database error has occured.<br/><br/>" + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblError.Text = "Another user may have updated that category. <br/><br/>" + "Please try again.";
        }
    }

    /// <summary>
    ///     Handles the RowDeleted event of the gvSoftware control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewDeletedEventArgs" /> instance containing the event data.</param>
    protected void gvSoftware_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A database error has occured. <br/><br/>" + "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblError.Text = "Another user may have updated that category. <br/><br/>" + "Please try again.";
        }
    }
}