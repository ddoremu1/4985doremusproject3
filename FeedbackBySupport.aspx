﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FeedbackBySupport.aspx.cs" Inherits="FeedbackBySupport" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="DUMBPlaceHolder" Runat="Server">
    <br/>
    <asp:DropDownList ID="ddlSupportFeedback" runat="server" DataSourceID="odsSupportFeedbackDropDownList" AutoPostBack="True" DataTextField="Name" DataValueField="SupportID"></asp:DropDownList>
    <asp:ObjectDataSource ID="odsSupportFeedbackDropDownList" runat="server" SelectMethod="GetAllSupportStaff" TypeName="SupportDatabase"></asp:ObjectDataSource>
    <br/>
    <br/>
    <asp:GridView ID="gvSupportFeedback" runat="server" DataSourceID="odsGridView" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White"/>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
        <HeaderStyle BackColor="#8ABC00" Font-Bold="True" ForeColor="White"/>
        <PagerStyle BackColor="lightgrey" ForeColor="#333333" HorizontalAlign="Center"/>
        <RowStyle BackColor="lightgrey" ForeColor="#333333"/>
        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy"/>
        <SortedAscendingCellStyle BackColor="#FDF5AC"/>
        <SortedAscendingHeaderStyle BackColor="#4D0000"/>
        <SortedDescendingCellStyle BackColor="#FCF6C0"/>
        <SortedDescendingHeaderStyle BackColor="#820000"/>
    </asp:GridView>
    <asp:ObjectDataSource ID="odsGridView" runat="server" SelectMethod="GetOpenFeedbackIncidents" TypeName="FeedbackDatabase">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlSupportFeedback" Name="supportStaffId" PropertyName="SelectedValue" Type="Int32"/>
        </SelectParameters>
    </asp:ObjectDataSource>
    <br/>
</asp:Content>