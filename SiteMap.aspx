﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SiteMap.aspx.cs" Inherits="SiteMap" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="DUMBPlaceHolder" Runat="Server">
    <asp:TreeView ID="tvNavigation" runat="server" DataSourceID="smdsSiteMapNav" ImageSet="Arrows">
        <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD"/>
        <NodeStyle Font-Names="Tahoma" Font-Size="10pt" ForeColor="Black" HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px"/>
        <ParentNodeStyle Font-Bold="False"/>
        <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px" VerticalPadding="0px"/>
    </asp:TreeView>
    <asp:SiteMapDataSource ID="smdsSiteMapNav" runat="server"/>
</asp:Content>