﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainSupport.aspx.cs" Inherits="MaintainSupport" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="DUMBPlaceHolder" Runat="Server">
    <asp:Label ID="lblStaffSelection" runat="server" Text="Please select a staff member: "></asp:Label>
    <asp:DropDownList ID="ddlSupportStaff" runat="server" DataSourceID="sdsSupportStaff" DataTextField="Name" DataValueField="Name" AutoPostBack="True"></asp:DropDownList>
    <asp:SqlDataSource ID="sdsSupportStaff" runat="server" ConnectionString="<%$ ConnectionStrings:DigitalManagerConnectionString %>" ProviderName="<%$ ConnectionStrings:DigitalManagerConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Support] ORDER BY [Name]"></asp:SqlDataSource>
    <br/> <br/>
    <asp:FormView ID="fvSupportStaff" runat="server" DataKeyNames="SupportID" DataSourceID="sdsSupportStaffForm" CellPadding="4" ForeColor="white" OnItemDeleted="fvSupportStaff_ItemDeleted" OnItemUpdated="fvSupportStaff_ItemUpdated">
        <EditItemTemplate>
            <table>
                <tr>
                    <td>SupportID:</td>
                    <td>
                        <asp:Label ID="SupportIDLabel1" runat="server" Text='<%# Eval("SupportID") %>'/>
                    </td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td>
                        <asp:TextBox ID="NameTextBox" runat="server" Text='<%#
    Bind("Name") %>'/>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfvUpdateName" runat="server" ErrorMessage="Required field." ControlToValidate="NameTextBox" Display="Dynamic">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td> Email:</td>
                    <td>
                        <asp:TextBox ID="EmailTextBox" runat="server" Text='<%#
    Bind("Email") %>'/>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfvUpdateEmail" runat="server" ErrorMessage="Required field." ControlToValidate="EmailTextBox" Display="Dynamic">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revUpdateEmail" runat="server" ErrorMessage="Not a proper email address." ControlToValidate="EmailTextBox" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>Phone:</td>
                    <td>
                        <asp:TextBox ID="PhoneTextBox" runat="server" Text='<%#
    Bind("Phone") %>'/>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfvUpdatePhone" runat="server" ErrorMessage="Required field." ControlToValidate="PhoneTextBox" Display="Dynamic">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revUpdatePhone" runat="server" ControlToValidate="PhoneTextBox" Display="Dynamic" ErrorMessage="Not a proper phone number." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"> <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update"/>&nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"/></td>
                </tr>
            </table>
        </EditItemTemplate>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
        <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
        <InsertItemTemplate>
            <table>
                <tr>
                    <td>SupportID:</td>
                    <td>
                        <asp:TextBox ID="SupportIDTextBox" runat="server" Text='<%#
    Bind("SupportID") %>'/>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfvInsertSupportId" runat="server" ErrorMessage="Required field." ControlToValidate="SupportIDTextBox" Display="Dynamic">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="cvInsertSupportId" runat="server" ErrorMessage="Not a proper ID" ControlToValidate="SupportIDTextBox" Display="Dynamic" Operator="DataTypeCheck" Type="Integer">*</asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td>
                        <asp:TextBox ID="NameTextBox" runat="server" Text='<%#
    Bind("Name") %>'/>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfvInsertName" runat="server" ErrorMessage="Required field." ControlToValidate="NameTextBox" Display="Dynamic">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td> Email:</td>
                    <td>
                        <asp:TextBox ID="EmailTextBox" runat="server" Text='<%#
    Bind("Email") %>'/>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfvInsertEmail" runat="server" ErrorMessage="Required field." ControlToValidate="EmailTextBox" Display="Dynamic">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revInsertEmail" runat="server" ErrorMessage="Not a proper email." ControlToValidate="EmailTextBox" Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>Phone:</td>
                    <td>
                        <asp:TextBox ID="PhoneTextBox" runat="server" Text='<%#
    Bind("Phone") %>'/>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfvInsertPhone" runat="server" ErrorMessage="Required field." ControlToValidate="PhoneTextBox" Display="Dynamic">*</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="revInsertPhone" runat="server" ErrorMessage="Not a proper phone number." ControlToValidate="PhoneTextBox" Display="Dynamic" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"> <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert"/>&nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"/></td>
                </tr>
            </table>
        </InsertItemTemplate>
        <ItemTemplate>
            <table>
                <tr>
                    <td> SupportID:</td>
                    <td>
                        <asp:Label ID="SupportIDLabel" runat="server" Text='<%# Eval("SupportID") %>'/>
                    </td>
                </tr>

                <tr>
                    <td> Name:</td>
                    <td>
                        <asp:Label ID="NameLabel" runat="server" Text='<%#
    Bind("Name") %>'/>
                    </td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td>
                        <asp:Label ID="EmailLabel" runat="server" Text='<%#
    Bind("Email") %>'/>
                    </td>
                </tr>
                <tr>
                    <td> Phone:</td>
                    <td>
                        <asp:Label ID="PhoneLabel" runat="server" Text='<%#
    Bind("Phone") %>'/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:LinkButton ID="lbtnUpdate" runat="server" OnClick="lbtnUpdate_Click">Update</asp:LinkButton> <asp:LinkButton ID="lbtnCreate" runat="server" OnClick="lbtnCreate_Click">Insert</asp:LinkButton> <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
        <PagerStyle BackColor="#FFCC66" ForeColor="white" HorizontalAlign="Center"/>
        <RowStyle BackColor="lightgrey" ForeColor="white"/>
    </asp:FormView>
    <asp:SqlDataSource ID="sdsSupportStaffForm" runat="server" ConnectionString="<%$ ConnectionStrings:DigitalManagerConnectionString %>" ProviderName="<%$ ConnectionStrings:DigitalManagerConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Support] WHERE ([Name] = ?)" ConflictDetection="CompareAllValues" DeleteCommand="DELETE FROM [Support] WHERE [SupportID] = ? AND [Name] = ? AND [Email] = ? AND [Phone] = ?" InsertCommand="INSERT INTO [Support] ([SupportID], [Name], [Email], [Phone]) VALUES (?, ?, ?, ?)" OldValuesParameterFormatString="original_{0}" UpdateCommand="UPDATE [Support] SET [Name] = ?, [Email] = ?, [Phone] = ? WHERE [SupportID] = ? AND [Name] = ? AND [Email] = ? AND [Phone] = ?">
        <DeleteParameters>
            <asp:Parameter Name="original_SupportID" Type="Int32"/>
            <asp:Parameter Name="original_Name" Type="String"/>
            <asp:Parameter Name="original_Email" Type="String"/>
            <asp:Parameter Name="original_Phone" Type="String"/>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="SupportID" Type="Int32"/>
            <asp:Parameter Name="Name" Type="String"/>
            <asp:Parameter Name="Email" Type="String"/>
            <asp:Parameter Name="Phone" Type="String"/>
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlSupportStaff" Name="Name" PropertyName="SelectedValue" Type="String"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String"/>
            <asp:Parameter Name="Email" Type="String"/>
            <asp:Parameter Name="Phone" Type="String"/>
            <asp:Parameter Name="original_SupportID" Type="Int32"/>
            <asp:Parameter Name="original_Name" Type="String"/>
            <asp:Parameter Name="original_Email" Type="String"/>
            <asp:Parameter Name="original_Phone" Type="String"/>
        </UpdateParameters>
    </asp:SqlDataSource>

    <br/>
    <asp:ValidationSummary ID="vsErrors" runat="server" HeaderText="Please fix the following errors:"/>

    <br/>
    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>

</asp:Content>