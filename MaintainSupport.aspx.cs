﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Code behind for MaintainSupport class
/// </summary>
/// <author>
/// Danl Doremus
/// </author>
/// <version>
/// Spring 2015
/// </version>
public partial class MaintainSupport : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.ddlSupportStaff.DataBind();
        }
    }

    /// <summary>
    /// Handles the Click event of the lbtnUpdate control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void lbtnUpdate_Click(object sender, EventArgs e)
    {
        this.fvSupportStaff.ChangeMode(FormViewMode.Edit);
    }

    /// <summary>
    /// Handles the Click event of the lbtnCreate control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void lbtnCreate_Click(object sender, EventArgs e)
    {
        this.fvSupportStaff.ChangeMode(FormViewMode.Insert);
    }

    /// <summary>
    /// Handles the ItemUpdated event of the fvSupportStaff control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="FormViewUpdatedEventArgs" /> instance containing the event data.</param>
    protected void fvSupportStaff_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A database error has occured.<br/><br/>" + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblError.Text = "Another user may have updated that category. <br/><br/>" + "Please try again.";
        }
        else
        {
            this.ddlSupportStaff.DataBind();
        }
    }

    /// <summary>
    /// Handles the ItemDeleted event of the fvSupportStaff control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="FormViewDeletedEventArgs" /> instance containing the event data.</param>
    protected void fvSupportStaff_ItemDeleted(object sender, FormViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A database error has occured. <br/><br/>" + "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblError.Text = "Another user may have updated that category. <br/><br/>" + "Please try again.";
        }
        else
        {
            this.ddlSupportStaff.DataBind();
        }
    }
}