﻿using System;
using System.Web.UI;

/// <summary>
///     Code behind feedback complete page
/// </summary>
/// <version>
///     Spring 2015
/// </version>
/// <author>
///     Danl Doremus
/// </author>
public partial class FeedbackComplete : Page
{
    /// <summary>
    ///     Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ContactCustomer"] == null)
        {
            return;
        }
        if (Session["ContactCustomer"].Equals(true))
        {
            this.lblContactingCustomer.Text = "Someone from our company will be contacting you soon.";
        }
    }
}