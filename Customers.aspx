﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Customers.aspx.cs" Inherits="Customers" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="DUMBPlaceHolder" Runat="Server">
    <asp:DropDownList ID="ddlCustomers" runat="server" DataSourceID="dsDigitalManager" DataTextField="Name" DataValueField="Name" AutoPostBack="True"></asp:DropDownList>
    <asp:SqlDataSource ID="dsDigitalManager" runat="server" ConnectionString="<%$ ConnectionStrings:DigitalManagerConnectionString %>" ProviderName="<%$ ConnectionStrings:DigitalManagerConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Customer] ORDER BY [Name]"></asp:SqlDataSource>
    <br/>
    <asp:Label ID="lblName" runat="server" Text="Name: "></asp:Label>
    <asp:Label ID="lblNameResults" runat="server" Text=""></asp:Label>
    <br/>
    <asp:Label ID="lblAddress" runat="server" Text="Address: "></asp:Label>
    <asp:Label ID="lblAddressResults" runat="server" Text=""></asp:Label>
    <br/>
    <asp:Label ID="lblCity" runat="server" Text="City: "></asp:Label>
    <asp:Label ID="lblCityResults" runat="server" Text=""></asp:Label>
    <br/>
    <asp:Label ID="lblState" runat="server" Text="State: "></asp:Label>
    <asp:Label ID="lblStateResults" runat="server" Text=""></asp:Label>
    <br/>
    <asp:Label ID="lblZipCode" runat="server" Text="Zip Code: "></asp:Label>
    <asp:Label ID="lblZipCodeResults" runat="server" Text=""></asp:Label>
    <br/>
    <asp:Label ID="lblPhone" runat="server" Text="Phone: "></asp:Label>
    <asp:Label ID="lblPhoneResults" runat="server" Text=""></asp:Label>
    <br/>
    <asp:Label ID="lblEmail" runat="server" Text="Email: "></asp:Label>
    <asp:Label ID="lblEmailResults" runat="server" Text=""></asp:Label>
    <br/>
    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    <br/>
    <asp:Button ID="btnViewContactList" runat="server" Text="View Contact List" OnClick="btnViewContactList_Click"/>
    <asp:Button ID="btnAddToContacts" runat="server" Text="Add to Contacts" OnClick="btnAddToContacts_Click"/>
</asp:Content>