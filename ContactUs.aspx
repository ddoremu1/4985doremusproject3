﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="ContactUs" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="DUMBPlaceHolder" Runat="Server">
    <asp:Label ID="lblContactUs" runat="server" Text="Contact Us"></asp:Label>
    <br/>
    <asp:Label ID="lblPhoneNumber" runat="server" Text="Phone number: "></asp:Label><asp:HyperLink ID="hlPhone" runat="server" NavigateUrl="tel:88-BallGame">88-BallGame</asp:HyperLink>
    <br/>
    <asp:Label ID="lblHours" runat="server" Text="Hours of operation: (Monday - Friday 9-5)"></asp:Label>
    <br/>
    <asp:Label ID="lblEmail" runat="server" Text="Email address: "></asp:Label> <asp:HyperLink ID="hlEmail" runat="server" NavigateUrl="mailto:info@ballgame.com">info@ballgame.com</asp:HyperLink>
    <br/>
    <asp:Label ID="lblAddress" runat="server" Text="Address: 6856 Orchard Avenue 
Mishawaka, IN 46544">
    </asp:Label>
</asp:Content>