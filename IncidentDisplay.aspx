﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="IncidentDisplay.aspx.cs" Inherits="IncidentDisplay" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" Runat="Server">
    <link href="Styles/IncidentDisplay.css" rel="stylesheet" type="text/css"/>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="DUMBPlaceHolder" Runat="Server">
    <asp:Label ID="lblIncidentDisplay" runat="server" Text="Choose a customer: "></asp:Label>
    <asp:DropDownList ID="ddlIncidentDisplay" runat="server" DataSourceID="dsDigitalManagerDropDownList" DataTextField="Name" DataValueField="CustomerID" AutoPostBack="True"></asp:DropDownList>
    <br/>
    <asp:SqlDataSource ID="dsDigitalManagerDropDownList" runat="server" ConnectionString="<%$ ConnectionStrings:DigitalManagerConnectionString %>" ProviderName="<%$ ConnectionStrings:DigitalManagerConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Customer] ORDER BY [Name]"></asp:SqlDataSource>
    <br/><br/>
    <asp:DataList ID="dlIncidentDisplay" runat="server" DataSourceID="dsIncidentDisplayDataList">
        <HeaderStyle BackColor="#8ABC00" ForeColor="White"></HeaderStyle>
        <HeaderTemplate>
            <table>
                <tr>
                    <th>Incident</th>
                    <th>Technician ID</th>
                    <th>Date Opened</th>
                    <th>Date Closed</th>
                </tr>
            </table>

        </HeaderTemplate>

        <ItemTemplate>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>'/>
                    </td>
                    <td>
                        <asp:Label ID="SupportIDLabel" runat="server" Text='<%# Eval("SupportID") %>'/>
                    </td>
                    <td>
                        <asp:Label ID="DateOpenedLabel" runat="server" Text='<%# Eval("DateOpened") %>'/>
                    </td>
                    <td>
                        <asp:Label ID="DateClosedLabel" runat="server" Text='<%# Eval("DateClosed") %>'/>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Label ID="DescriptionLabel" runat="server" Text='<%# Eval("Description") %>'/>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
        <AlternatingItemStyle BackColor="#8ABC00" ForeColor="White"/>
    </asp:DataList>
    <asp:SqlDataSource ID="dsIncidentDisplayDataList" runat="server" ConnectionString="<%$ ConnectionStrings:DigitalManagerConnectionString %>" ProviderName="<%$ ConnectionStrings:DigitalManagerConnectionString.ProviderName %>" SelectCommand="SELECT [DateOpened], [SupportID], [DateClosed], [Description], [Title], [CustomerID] FROM [Feedback] WHERE ([CustomerID] = @CustomerID) ORDER BY [DateOpened]">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlIncidentDisplay" Name="Name" PropertyName="SelectedValue" Type="String"/>
        </SelectParameters>
    </asp:SqlDataSource>
    <br/>
</asp:Content>