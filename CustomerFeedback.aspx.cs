﻿using System;
using System.Data;
using System.Web.UI;

/// <summary>
/// Customer feedback
/// </summary>
/// <author>
/// Danl Doremus
/// </author>
/// <version>
/// Spring 2015
/// </version>
public partial class CustomerFeedback : Page
{
    /// <summary>
    /// The _customer feedback
    /// </summary>
    private Feedback _customerFeedback;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.lstClosedFeedback.Items.Clear();
        }

        SetFocus(this.txtCustomerId);
        this.rfvContactMethod.Enabled = false;
    }

    /// <summary>
    /// Handles the Click event of the btnCustomerId control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnCustomerId_Click(object sender, EventArgs e)
    {
        SetFocus(this.lstClosedFeedback);
        if (this.txtCustomerId.Text == "")
        {
            return;
        }

        uint number;

        var result = UInt32.TryParse(this.txtCustomerId.Text, out number);

        if (!result)
        {
            return;
        }

        var feedbackTable = (DataView) this.sdsFeedback.Select(DataSourceSelectArguments.Empty);

        if (feedbackTable == null)
        {
            return;
        }
        feedbackTable.RowFilter = "CustomerID = '" + this.txtCustomerId.Text + "'";

        this.SendFeedbackToListBox(feedbackTable);

        if (feedbackTable.Count > 0)
        {
            this.ControlEnabling(true);
        }
        else
        {
            this.ControlEnabling(false);
        }
    }

    /// <summary>
    /// Controls the enabling.
    /// </summary>
    /// <param name="enabled">if set to <c>true</c> [enabled].</param>
    private void ControlEnabling(bool enabled)
    {
        this.rblServiceTime.Enabled = enabled;
        this.rblTechnicalEfficiency.Enabled = enabled;
        this.rblResolution.Enabled = enabled;
        this.txtAdditionalComments.Enabled = enabled;
        this.cbContact.Enabled = enabled;
        this.btnSubmit.Enabled = enabled;
    }

    /// <summary>
    /// Sends the feedback to ListBox.
    /// </summary>
    /// <param name="feedbackTable">The feedback table.</param>
    private void SendFeedbackToListBox(DataView feedbackTable)
    {
        for (var i = 0; i < feedbackTable.Count; i++)
        {
            var row = feedbackTable[i];

            this._customerFeedback = new Feedback();

            if (row != null)
            {
                this._customerFeedback.FeedbackId = row["FeedbackID"].ToString();
                this._customerFeedback.CustomerId = row["CustomerID"].ToString();
                this._customerFeedback.SoftwareId = row["SoftwareID"].ToString();
                this._customerFeedback.SupportId = row["SupportID"].ToString();
                this._customerFeedback.DateOpened = row["DateOpened"].ToString();
                this._customerFeedback.DateClosed = row["DateClosed"].ToString();
                this._customerFeedback.Title = row["Title"].ToString();
                this._customerFeedback.Description = row["Description"].ToString();
            }

            this.lstClosedFeedback.Items.Add(this._customerFeedback.FormatFeedback());
        }
    }

    /// <summary>
    /// Handles the Click event of the btnSubmit control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
        {
            return;
        }
        var customerFeedback = new Description
        {
            CustomerId = this.txtCustomerId.Text,
            ServiceTime = this.rblServiceTime.SelectedIndex,
            Efficiency = this.rblTechnicalEfficiency.SelectedIndex,
            Resolution = this.rblResolution.SelectedIndex,
            Comments = this.txtAdditionalComments.Text,
            Contact = this.cbContact.Checked,
            ContactMethod = this.rblContactMethod.SelectedIndex
        };

        Session["ContactCustomer"] = this.cbContact.Checked;
        Session["CustomerFeedback"] = customerFeedback;

        Response.Redirect("FeedbackComplete.aspx");
    }

    /// <summary>
    /// Handles the CheckedChanged event of the cbContact control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void cbContact_CheckedChanged(object sender, EventArgs e)
    {
        if (this.cbContact.Checked)
        {
            this.rblContactMethod.Enabled = true;
            this.rfvContactMethod.Enabled = true;
        }
        else
        {
            this.rblContactMethod.Enabled = false;
            this.rblContactMethod.ClearSelection();
            this.rfvContactMethod.Enabled = false;
        }
    }
}