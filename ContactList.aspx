﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ContactList.aspx.cs" Inherits="ContactList" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" Runat="Server">
    <link href="Styles/ContactList.css" rel="stylesheet" type="text/css"/>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="DUMBPlaceHolder" Runat="Server">
    <asp:Label ID="lblContactList" runat="server" Text="Contact List"></asp:Label>
    <br/>
    <asp:ListBox ID="lstContactList" runat="server" CssClass="contactList"></asp:ListBox>
    <br/>
    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    <br/>
    <asp:Button ID="btnSelectAdditionalCustomers" runat="server" Text="Select Additional Customers" OnClick="btnSelectAdditionalCustomers_Click" CssClass="buttons"/><asp:Button ID="btnRemoveCustomer" runat="server" Text="Remove Customer" OnClick="btnRemoveCustomer_Click" CssClass="buttons"/><asp:Button ID="btnClearList" runat="server" Text="Clear List" OnClick="btnClearList_Click" CssClass="buttons"/>
</asp:Content>