﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Software.aspx.cs" Inherits="Software" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" Runat="Server">
    <link href="Styles/Software.css" rel="stylesheet" type="text/css"/>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="DUMBPlaceHolder" Runat="Server">
    <asp:GridView ID="gvSoftware" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataKeyNames="SoftwareID" DataSourceID="dsDigitalManagement" ForeColor="Black" GridLines="Horizontal" OnRowDeleted="gvSoftware_RowDeleted" OnRowUpdated="gvSoftware_RowUpdated">
        <Columns>
            <asp:BoundField DataField="SoftwareID" HeaderText="SoftwareID" ReadOnly="True" SortExpression="SoftwareID"></asp:BoundField>
            <asp:TemplateField HeaderText="Name" SortExpression="Name">
                <EditItemTemplate>
                    <asp:TextBox ID="txtSoftwareName" runat="server" Text='<%#
    Bind("Name") %>'>
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvSoftwareName" runat="server" ControlToValidate="txtSoftwareName" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#
    Bind("Name") %>'>
                    </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Version" SortExpression="Version">
                <EditItemTemplate>
                    <asp:TextBox ID="txtSoftwareVersion" runat="server" Text='<%#
    Bind("Version") %>'>
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvSoftwareVersion" runat="server" ControlToValidate="txtSoftwareVersion" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%#
    Bind("Version") %>'>
                    </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ReleaseDate" SortExpression="ReleaseDate">
                <EditItemTemplate>
                    <asp:TextBox ID="txtSoftwareReleaseDate" runat="server" Text='<%#
    Bind("ReleaseDate") %>'>
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvSoftwareReleaseDate" runat="server" ControlToValidate="txtSoftwareReleaseDate" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%#
    Bind("ReleaseDate") %>'>
                    </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True" ShowDeleteButton="True"/>
        </Columns>
        <FooterStyle BackColor="#CCCC99" ForeColor="Black"/>
        <HeaderStyle BackColor="#8ABC00" Font-Bold="True" ForeColor="White"/>
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right"/>
        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White"/>
        <SortedAscendingCellStyle BackColor="#F7F7F7"/>
        <SortedAscendingHeaderStyle BackColor="#4B4B4B"/>
        <SortedDescendingCellStyle BackColor="#E5E5E5"/>
        <SortedDescendingHeaderStyle BackColor="#242121"/>
    </asp:GridView>
    <asp:SqlDataSource ID="dsDigitalManagement" runat="server"
                       ConnectionString="<%$ ConnectionStrings:DigitalManagerConnectionString %>"
                       ConflictDetection="CompareAllValues"
                       OldValuesParameterFormatString="original_{0}"
                       ProviderName="<%$ ConnectionStrings:DigitalManagerConnectionString.ProviderName %>"
                       SelectCommand="SELECT * FROM [Software] ORDER BY [SoftwareID]"
                       DeleteCommand="DELETE FROM [Software] WHERE [SoftwareID] = @original_SoftwareID AND [Name] = @original_Name AND [Version] = @original_Version AND [ReleaseDate] = @original_ReleaseDate"
                       InsertCommand="INSERT INTO [Software] ([SoftwareID], [Name], [Version], [ReleaseDate]) VALUES (?, ?, ?, ?)"
                       UpdateCommand="UPDATE [Software] SET [Name] = ?, [Version] = ?, [ReleaseDate] = ? WHERE [SoftwareID] = ?">
        <DeleteParameters>
            <asp:Parameter Name="original_SoftwareID" Type="String"/>
            <asp:Parameter Name="original_Name" Type="String"/>
            <asp:Parameter Name="original_Version" Type="Decimal"/>
            <asp:Parameter Name="original_ReleaseDate" Type="DateTime"/>
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="SoftwareID" Type="String"/>
            <asp:Parameter Name="Name" Type="String"/>
            <asp:Parameter Name="Version" Type="Decimal"/>
            <asp:Parameter Name="ReleaseDate" Type="DateTime"/>
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Name" Type="String"/>
            <asp:Parameter Name="Version" Type="Decimal"/>
            <asp:Parameter Name="ReleaseDate" Type="DateTime"/>
            <asp:Parameter Name="SoftwareID" Type="String"/>
        </UpdateParameters>
    </asp:SqlDataSource>
    <br/>
    <asp:ValidationSummary ID="vsSoftwareGridView" runat="server" HeaderText="Please fix the following errors:"/>
    <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
    <br/>
    <br/>
    <asp:Label ID="lblAddingNewData" runat="server" Text="Add new software below:"></asp:Label>
    <br/>
    <br/>
    <asp:Label ID="lblIdTitle" runat="server" Text="Software ID:"></asp:Label>
    <br/>
    <asp:TextBox ID="txtNewSoftwareId" runat="server"></asp:TextBox>
    <br/>
    <asp:RequiredFieldValidator ID="rfvSoftwareId" runat="server" ControlToValidate="txtNewSoftwareId" Display="Dynamic" ErrorMessage="Required field." ValidationGroup="newSoftwareObject">*</asp:RequiredFieldValidator>
    <br/>
    <asp:Label ID="lblName" runat="server" Text="Name:"></asp:Label>
    <br/>
    <asp:TextBox ID="txtNewSoftwareName" runat="server"></asp:TextBox>
    <br/>
    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtNewSoftwareName" Display="Dynamic" ErrorMessage="Required field." ValidationGroup="newSoftwareObject">*</asp:RequiredFieldValidator>
    <br/>
    <asp:Label ID="lblVersion" runat="server" Text="Version:"></asp:Label>
    <br/>
    <asp:TextBox ID="txtNewSoftwareVersion" runat="server"></asp:TextBox>
    <br/>
    <asp:CompareValidator ID="cvVersion" runat="server" ControlToValidate="txtNewSoftwareVersion" Display="Dynamic" ErrorMessage="Not proper data." Type="Double" ValidationGroup="newSoftwareObject" Operator="DataTypeCheck">*</asp:CompareValidator>
    <asp:RequiredFieldValidator ID="rfvVersion" runat="server" ControlToValidate="txtNewSoftwareVersion" Display="Dynamic" ErrorMessage="Required field." ValidationGroup="newSoftwareObject">*</asp:RequiredFieldValidator>
    <br/>
    <asp:Label ID="lblReleaseDate" runat="server" Text="Release Date:"></asp:Label>
    <br/>
    <asp:TextBox ID="txtNewSoftwareReleaseDate" runat="server"></asp:TextBox>
    <br/>
    <asp:CompareValidator ID="cvReleaseData" runat="server" ControlToValidate="txtNewSoftwareReleaseDate" Display="Dynamic" ErrorMessage="Not proper data." Type="Date" ValidationGroup="newSoftwareObject" Operator="DataTypeCheck">*</asp:CompareValidator>
    <asp:RequiredFieldValidator ID="rfvReleaseDate" runat="server" ControlToValidate="txtNewSoftwareReleaseDate" Display="Dynamic" ErrorMessage="Required field." ValidationGroup="newSoftwareObject">*</asp:RequiredFieldValidator>
    <br/>
    <asp:Button ID="btnAddSoftware" runat="server" Text="Add" OnClick="btnAddSoftware_Click" ValidationGroup="newSoftwareObject"/>
    <br/>
    <br/>
    <asp:ValidationSummary ID="vsNewSoftware" runat="server" HeaderText="Please fix the following errors:" ValidationGroup="newSoftwareObject"/>
</asp:Content>