﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CustomerFeedback.aspx.cs" Inherits="CustomerFeedback" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" Runat="Server">
    <link href="Styles/Feedback.css" rel="stylesheet" type="text/css"/>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="DUMBPlaceHolder" Runat="Server">
    <asp:SqlDataSource ID="sdsFeedback" runat="server" ConnectionString="<%$ ConnectionStrings:DigitalManagerConnectionString %>" ProviderName="<%$ ConnectionStrings:DigitalManagerConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Feedback] WHERE ([DateClosed] IS NOT NULL)"></asp:SqlDataSource>
    <br/>

    <asp:Label ID="lblCustomerId" runat="server" Text="Customer ID: "></asp:Label>
    <asp:TextBox ID="txtCustomerId" runat="server"></asp:TextBox>
    <asp:Button ID="btnCustomerId" runat="server" Text="Submit ID" OnClick="btnCustomerId_Click" CausesValidation="False"/>
    <br/>
    <asp:RequiredFieldValidator ID="rfvCustomerId" runat="server" ErrorMessage="Please enter a customer ID." ControlToValidate="txtCustomerId" Display="Dynamic"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="vcCustomerId" runat="server" ErrorMessage="Please enter a valid customer ID." ControlToValidate="txtCustomerId" Operator="DataTypeCheck" Type="Integer" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
    <br/>
    <asp:ListBox CssClass="lstClosedFeedback" ID="lstClosedFeedback" runat="server" TabIndex="1" AutoPostBack="True"></asp:ListBox>
    <br/>
    <asp:RequiredFieldValidator ID="rfvClosedFeedback" runat="server" ErrorMessage="Please select an issue." ControlToValidate="lstClosedFeedback" Display="Dynamic"></asp:RequiredFieldValidator>
    <br/>
    <asp:Label ID="lblServiceTime" runat="server" Text="Service Time: "></asp:Label>
    <asp:RadioButtonList ID="rblServiceTime" runat="server" RepeatDirection="Horizontal" Enabled="False" CssClass="ratingButtons" AutoPostBack="True">
        <asp:ListItem Value="1">Satisfied</asp:ListItem>
        <asp:ListItem Value="2">Neither Satisified Nor Dissatisfied</asp:ListItem>
        <asp:ListItem Value="3">Dissatisfied</asp:ListItem>
    </asp:RadioButtonList>
    <asp:RequiredFieldValidator ID="rfvServiceTime" runat="server" ControlToValidate="rblServiceTime" ErrorMessage="Please select a rating."></asp:RequiredFieldValidator>
    <br/>
    <asp:Label ID="lblTechnicalEfficiency" runat="server" Text="Technical efficiency: "></asp:Label>

    <asp:RadioButtonList ID="rblTechnicalEfficiency" runat="server" RepeatDirection="Horizontal" Enabled="False" CssClass="ratingButtons" AutoPostBack="True">
        <asp:ListItem Value="1">Satisfied</asp:ListItem>
        <asp:ListItem Value="2">Neither Satisified Nor Dissatisfied</asp:ListItem>
        <asp:ListItem Value="3">Dissatisfied</asp:ListItem>
    </asp:RadioButtonList>
    <asp:RequiredFieldValidator ID="rfvEfficiency" runat="server" ControlToValidate="rblTechnicalEfficiency" ErrorMessage="Please select a rating."></asp:RequiredFieldValidator>
    <br/>
    <asp:Label ID="lblProblemResolution" runat="server" Text="Problem Resolution: "></asp:Label>

    <asp:RadioButtonList ID="rblResolution" runat="server" RepeatDirection="Horizontal" Enabled="False" CssClass="ratingButtons" AutoPostBack="True">
        <asp:ListItem Value="1">Satisfied</asp:ListItem>
        <asp:ListItem Value="2">Neither Satisified Nor Dissatisfied</asp:ListItem>
        <asp:ListItem Value="3">Dissatisfied</asp:ListItem>
    </asp:RadioButtonList>
    <asp:RequiredFieldValidator ID="rfvResolution" runat="server" ControlToValidate="rblResolution" ErrorMessage="Please select a rating."></asp:RequiredFieldValidator>
    <br/>
    <asp:Label ID="lblAdditionalComments" runat="server" Text="Additional comments: "></asp:Label>
    <br/>
    <asp:TextBox ID="txtAdditionalComments" runat="server" Enabled="False" CssClass="additonalFeedback" TextMode="MultiLine"></asp:TextBox>
    <br/>
    <asp:CheckBox ID="cbContact" runat="server" Text="Would you like to be contacted by us?" Enabled="False" AutoPostBack="True" OnCheckedChanged="cbContact_CheckedChanged"/>
    <br/>
    <br/>
    <asp:Label ID="lblContactMethod" runat="server" Text="Contact method: "></asp:Label>
    <asp:RadioButtonList ID="rblContactMethod" runat="server" RepeatDirection="Horizontal" Enabled="False" CssClass="contactMethodButtons" AutoPostBack="True">
        <asp:ListItem Value="0">Email</asp:ListItem>
        <asp:ListItem Value="1">Phone</asp:ListItem>
    </asp:RadioButtonList>
    <asp:RequiredFieldValidator ID="rfvContactMethod" runat="server" ControlToValidate="rblContactMethod" ErrorMessage="Please select a contact method."></asp:RequiredFieldValidator>
    <br/>
    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="submit" OnClick="btnSubmit_Click"/>
</asp:Content>