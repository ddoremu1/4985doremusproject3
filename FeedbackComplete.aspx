﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FeedbackComplete.aspx.cs" Inherits="FeedbackComplete" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="DUMBPlaceHolder" Runat="Server">
    <asp:Label ID="lblFeedbackConfirmation" runat="server" Text="Thank you! Your feedback has been received."></asp:Label>
    <br/>
    <br/>
    <asp:Label ID="lblContactingCustomer" runat="server" Text=""></asp:Label>
</asp:Content>