﻿using System;
using System.Data;
using System.Diagnostics;
using System.Web.UI;

/// <summary>
/// Code behind customer page
/// </summary>
/// <author>
/// Danl Doremus
/// </author>
/// <version>
/// Spring 2015
/// </version>
public partial class Customers : Page
{
    /// <summary>
    /// The _selected customer
    /// </summary>
    private Customer _selectedCustomer;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.ddlCustomers.DataBind();
        }

        this._selectedCustomer = this.GetSelectedCustomer();

        this.lblNameResults.Text = this._selectedCustomer.Name;
        this.lblAddressResults.Text = this._selectedCustomer.Address;
        this.lblCityResults.Text = this._selectedCustomer.City;
        this.lblStateResults.Text = this._selectedCustomer.State;
        this.lblZipCodeResults.Text = this._selectedCustomer.ZipCode;
        this.lblPhoneResults.Text = this._selectedCustomer.Phone;
        this.lblEmailResults.Text = this._selectedCustomer.Email;
    }

    /// <summary>
    /// Gets the selected customer.
    /// </summary>
    /// <returns></returns>
    private Customer GetSelectedCustomer()
    {
        var customersTable = (DataView) this.dsDigitalManager.Select(DataSourceSelectArguments.Empty);
        Debug.Assert(customersTable != null, "customersTable != null");
        customersTable.RowFilter = string.Format("Name = '{0}'", this.ddlCustomers.SelectedValue);
        var row = customersTable[0];

        var customer = new Customer
        {
            CustomerId = row["CustomerID"].ToString(),
            Name = row["Name"].ToString(),
            Address = row["Address"].ToString(),
            City = row["City"].ToString(),
            State = row["State"].ToString(),
            ZipCode = row["ZipCode"].ToString(),
            Phone = row["Phone"].ToString(),
            Email = row["Email"].ToString()
        };
        return customer;
    }

    /// <summary>
    /// Handles the Click event of the btnViewContactList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnViewContactList_Click(object sender, EventArgs e)
    {
        Response.Redirect("ContactList.aspx");
    }

    /// <summary>
    /// Handles the Click event of the btnAddToContacts control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnAddToContacts_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            var customerList = CustomerList.GetCustomers();
            var customerListItem = customerList[this._selectedCustomer.Name];

            if (customerListItem == null)
            {
                customerList.AddItem(this._selectedCustomer);
                Response.Redirect("ContactList.aspx");
            }
            else
            {
                this.lblMessage.Text = "Customer's information already in contact list.";
            }
        }
    }
}