﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CustomerMaintenance.aspx.cs" Inherits="CustomerMaintenance" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" Runat="Server">
    <link href="Styles/CustomerMaintentance.css" rel="stylesheet" type="text/css"/>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="DUMBPlaceHolder" Runat="Server">
<asp:GridView ID="gvCustomerMaintenance" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="CustomerID" DataSourceID="sdsGridViewCustomerMaintenance" PageSize="6" CellPadding="4" ForeColor="#333333" GridLines="None">
    <AlternatingRowStyle BackColor="White"/>
    <Columns>
        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"/>
        <asp:BoundField DataField="City" HeaderText="City" SortExpression="City"/>
        <asp:BoundField DataField="State" HeaderText="State" SortExpression="State"/>
        <asp:CommandField ShowSelectButton="True"/>
    </Columns>
    <FooterStyle BackColor="#586164" Font-Bold="True" ForeColor="White"/>
    <HeaderStyle BackColor="#8ABC00" Font-Bold="True" ForeColor="White"/>
    <PagerStyle BackColor="#586164" ForeColor="#333333" HorizontalAlign="Center"/>
    <RowStyle BackColor="lightgrey" ForeColor="#333333"/>
    <SelectedRowStyle BackColor="#BDC58F" Font-Bold="True" ForeColor="White"/>
    <SortedAscendingCellStyle BackColor="#FDF5AC"/>
    <SortedAscendingHeaderStyle BackColor="#4D0000"/>
    <SortedDescendingCellStyle BackColor="#FCF6C0"/>
    <SortedDescendingHeaderStyle BackColor="#820000"/>
</asp:GridView>

<asp:SqlDataSource ID="sdsGridViewCustomerMaintenance" runat="server" ConnectionString="<%$ ConnectionStrings:DigitalManagerConnectionString %>" ProviderName="<%$ ConnectionStrings:DigitalManagerConnectionString.ProviderName %>" SelectCommand="SELECT [Name], [CustomerID], [City], [State] FROM [Customer] ORDER BY [Name]"></asp:SqlDataSource>
<br/>
<asp:DetailsView ID="dvCustomerMaintenance" runat="server" AutoGenerateRows="False" DataKeyNames="CustomerID" DataSourceID="sdsDetailsViewCustomerMaintenance" Height="50px" OnItemDeleted="dvCustomerMaintenance_ItemDeleted" OnItemInserted="dvCustomerMaintenance_ItemInserted" OnItemUpdated="dvCustomerMaintenance_ItemUpdated" Width="125px">
    <Fields>
        <asp:TemplateField HeaderText="CustomerID" SortExpression="CustomerID">
            <EditItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%# Eval("CustomerID") %>'></asp:Label>
            </EditItemTemplate>
            <InsertItemTemplate>
                <asp:TextBox ID="txtInsertCustomerID" runat="server" Text='<%#
    Bind("CustomerID") %>'>
                </asp:TextBox>
                <asp:CompareValidator ID="cvInsertCustomerID" runat="server" ControlToValidate="txtInsertCustomerID" Display="Dynamic" ErrorMessage="Not a valid customer id." Operator="DataTypeCheck" Type="Integer">*</asp:CompareValidator>
                <asp:RequiredFieldValidator ID="rfvInsertCustomerID" runat="server" ControlToValidate="txtInsertCustomerID" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </InsertItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label1" runat="server" Text='<%#
    Bind("CustomerID") %>'>
                </asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Name" SortExpression="Name">
            <EditItemTemplate>
                <asp:TextBox ID="txtEditName" runat="server" Text='<%#
    Bind("Name") %>'>
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEditName" runat="server" ControlToValidate="txtEditName" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </EditItemTemplate>
            <InsertItemTemplate>
                <asp:TextBox ID="txtInsertName" runat="server" Text='<%#
    Bind("Name") %>'>
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvInsertName" runat="server" ControlToValidate="txtInsertName" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </InsertItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label2" runat="server" Text='<%#
    Bind("Name") %>'>
                </asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Address" SortExpression="Address">
            <EditItemTemplate>
                <asp:TextBox ID="txtEditAddress" runat="server" Text='<%#
    Bind("Address") %>'>
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEditAddress" runat="server" ControlToValidate="txtEditAddress" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </EditItemTemplate>
            <InsertItemTemplate>
                <asp:TextBox ID="txtInsertAddress" runat="server" Text='<%#
    Bind("Address") %>'>
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvInsertAddress" runat="server" ControlToValidate="txtInsertAddress" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </InsertItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label3" runat="server" Text='<%#
    Bind("Address") %>'>
                </asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="City" SortExpression="City">
            <EditItemTemplate>
                <asp:TextBox ID="txtEditCity" runat="server" Text='<%#
    Bind("City") %>'>
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEditCity" runat="server" ControlToValidate="txtEditCity" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </EditItemTemplate>
            <InsertItemTemplate>
                <asp:TextBox ID="txtInsertCity" runat="server" Text='<%#
    Bind("City") %>'>
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvInsertCity" runat="server" ControlToValidate="txtInsertCity" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </InsertItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label4" runat="server" Text='<%#
    Bind("City") %>'>
                </asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="State" SortExpression="State">
            <EditItemTemplate>
                <asp:TextBox ID="txtEditState" runat="server" Text='<%#
    Bind("State") %>'>
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEditState" runat="server" ControlToValidate="txtEditState" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </EditItemTemplate>
            <InsertItemTemplate>
                <asp:TextBox ID="txtInsertState" runat="server" Text='<%#
    Bind("State") %>'>
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvInsertState" runat="server" ControlToValidate="txtInsertState" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </InsertItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label5" runat="server" Text='<%#
    Bind("State") %>'>
                </asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="ZipCode" SortExpression="ZipCode">
            <EditItemTemplate>
                <asp:TextBox ID="txtEditZipCode" runat="server" Text='<%#
    Bind("ZipCode") %>'>
                </asp:TextBox>
                <asp:RegularExpressionValidator ID="revEditZipCode" runat="server" ControlToValidate="txtEditZipCode" Display="Dynamic" ErrorMessage="Not a proper zip code." ValidationExpression="\d{5}(-\d{4})?">*</asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="rfvEditZipCode" runat="server" ControlToValidate="txtEditZipCode" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </EditItemTemplate>
            <InsertItemTemplate>
                <asp:TextBox ID="txtInsertZipCode" runat="server" Text='<%#
    Bind("ZipCode") %>'>
                </asp:TextBox>
                <asp:RegularExpressionValidator ID="revInsertZipCode" runat="server" ControlToValidate="txtInsertZipCode" Display="Dynamic" ErrorMessage="Not a proper zip code." ValidationExpression="\d{5}(-\d{4})?">*</asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="rfvInsertZipCode" runat="server" ControlToValidate="txtInsertZipCode" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </InsertItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label6" runat="server" Text='<%#
    Bind("ZipCode") %>'>
                </asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Phone" SortExpression="Phone">
            <EditItemTemplate>
                <asp:TextBox ID="txtEditPhone" runat="server" Text='<%#
    Bind("Phone") %>'>
                </asp:TextBox>
                <asp:RegularExpressionValidator ID="revEditPhone" runat="server" ControlToValidate="txtEditPhone" Display="Dynamic" ErrorMessage="Not a proper phone number." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="rfvEditPhone" runat="server" ControlToValidate="txtEditPhone" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </EditItemTemplate>
            <InsertItemTemplate>
                <asp:TextBox ID="txtInsertPhone" runat="server" Text='<%#
    Bind("Phone") %>'>
                </asp:TextBox>
                <asp:RegularExpressionValidator ID="revInsertPhone" runat="server" ControlToValidate="txtInsertPhone" Display="Dynamic" ErrorMessage="Not a proper phone number." ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="rfvInsertPhone" runat="server" ControlToValidate="txtInsertPhone" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </InsertItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label7" runat="server" Text='<%#
    Bind("Phone") %>'>
                </asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Email" SortExpression="Email">
            <EditItemTemplate>
                <asp:TextBox ID="txtEditEmail" runat="server" Text='<%#
    Bind("Email") %>'>
                </asp:TextBox>
                <asp:RegularExpressionValidator ID="revEditEmail" runat="server" ControlToValidate="txtEditEmail" Display="Dynamic" ErrorMessage="Not a proper email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="rfvEditEmail" runat="server" ControlToValidate="txtEditEmail" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </EditItemTemplate>
            <InsertItemTemplate>
                <asp:TextBox ID="txtInsertEmail" runat="server" Text='<%#
    Bind("Email") %>'>
                </asp:TextBox>
                <asp:RegularExpressionValidator ID="revInsertEmail" runat="server" ControlToValidate="txtInsertEmail" Display="Dynamic" ErrorMessage="Not a proper email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="rfvInsertEmail" runat="server" ControlToValidate="txtInsertEmail" Display="Dynamic" ErrorMessage="Required field.">*</asp:RequiredFieldValidator>
            </InsertItemTemplate>
            <ItemTemplate>
                <asp:Label ID="Label8" runat="server" Text='<%#
    Bind("Email") %>'>
                </asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True"/>
    </Fields>
</asp:DetailsView>
<asp:SqlDataSource ID="sdsDetailsViewCustomerMaintenance" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:DigitalManagerConnectionString %>" DeleteCommand="DELETE FROM [Customer] WHERE [CustomerID] = ? AND [Name] = ? AND [Address] = ? AND [City] = ? AND [State] = ? AND [ZipCode] = ? AND (([Phone] = ?) OR ([Phone] IS NULL AND ? IS NULL)) AND (([Email] = ?) OR ([Email] IS NULL AND ? IS NULL))" InsertCommand="INSERT INTO [Customer] ([CustomerID], [Name], [Address], [City], [State], [ZipCode], [Phone], [Email]) VALUES (?, ?, ?, ?, ?, ?, ?, ?)" OldValuesParameterFormatString="original_{0}" ProviderName="<%$ ConnectionStrings:DigitalManagerConnectionString.ProviderName %>" SelectCommand="SELECT * FROM [Customer] WHERE ([CustomerID] = ?)" UpdateCommand="UPDATE [Customer] SET [Name] = ?, [Address] = ?, [City] = ?, [State] = ?, [ZipCode] = ?, [Phone] = ?, [Email] = ? WHERE [CustomerID] = ? AND [Name] = ? AND [Address] = ? AND [City] = ? AND [State] = ? AND [ZipCode] = ? AND (([Phone] = ?) OR ([Phone] IS NULL AND ? IS NULL)) AND (([Email] = ?) OR ([Email] IS NULL AND ? IS NULL))">
    <DeleteParameters>
        <asp:Parameter Name="original_CustomerID" Type="Int32"/>
        <asp:Parameter Name="original_Name" Type="String"/>
        <asp:Parameter Name="original_Address" Type="String"/>
        <asp:Parameter Name="original_City" Type="String"/>
        <asp:Parameter Name="original_State" Type="String"/>
        <asp:Parameter Name="original_ZipCode" Type="String"/>
        <asp:Parameter Name="original_Phone" Type="String"/>
        <asp:Parameter Name="original_Phone" Type="String"/>
        <asp:Parameter Name="original_Email" Type="String"/>
        <asp:Parameter Name="original_Email" Type="String"/>
    </DeleteParameters>
    <InsertParameters>
        <asp:Parameter Name="CustomerID" Type="Int32"/>
        <asp:Parameter Name="Name" Type="String"/>
        <asp:Parameter Name="Address" Type="String"/>
        <asp:Parameter Name="City" Type="String"/>
        <asp:Parameter Name="State" Type="String"/>
        <asp:Parameter Name="ZipCode" Type="String"/>
        <asp:Parameter Name="Phone" Type="String"/>
        <asp:Parameter Name="Email" Type="String"/>
    </InsertParameters>
    <SelectParameters>
        <asp:ControlParameter ControlID="gvCustomerMaintenance" Name="CustomerID" PropertyName="SelectedValue" Type="Int32"/>
    </SelectParameters>
    <UpdateParameters>
        <asp:Parameter Name="Name" Type="String"/>
        <asp:Parameter Name="Address" Type="String"/>
        <asp:Parameter Name="City" Type="String"/>
        <asp:Parameter Name="State" Type="String"/>
        <asp:Parameter Name="ZipCode" Type="String"/>
        <asp:Parameter Name="Phone" Type="String"/>
        <asp:Parameter Name="Email" Type="String"/>
        <asp:Parameter Name="original_CustomerID" Type="Int32"/>
        <asp:Parameter Name="original_Name" Type="String"/>
        <asp:Parameter Name="original_Address" Type="String"/>
        <asp:Parameter Name="original_City" Type="String"/>
        <asp:Parameter Name="original_State" Type="String"/>
        <asp:Parameter Name="original_ZipCode" Type="String"/>
        <asp:Parameter Name="original_Phone" Type="String"/>
        <asp:Parameter Name="original_Phone" Type="String"/>
        <asp:Parameter Name="original_Email" Type="String"/>
        <asp:Parameter Name="original_Email" Type="String"/>
    </UpdateParameters>
</asp:SqlDataSource>
<br/>
<asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
<asp:ValidationSummary ID="vsCustomerMaintenance" runat="server" HeaderText="Please fix the following errors:"/>

</asp:Content>