﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Code behind CustomerMaintenance
/// </summary>
/// <author>
/// Danl Doremus
/// </author>
/// <version>
/// Spring 2015
/// </version>
public partial class CustomerMaintenance : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblError.Text = "";
    }

    /// <summary>
    /// Handles the ItemInserted event of the dvCustomerMaintenance control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DetailsViewInsertedEventArgs" /> instance containing the event data.</param>
    protected void dvCustomerMaintenance_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A database error has occured.<br/><br/>" + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInInsertMode = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblError.Text = "Another user may have updated that category. <br/><br/>" + "Please try again.";
        }
        else
        {
            this.gvCustomerMaintenance.DataBind();
        }
    }

    /// <summary>
    /// Handles the ItemDeleted event of the dvCustomerMaintenance control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DetailsViewDeletedEventArgs" /> instance containing the event data.</param>
    protected void dvCustomerMaintenance_ItemDeleted(object sender, DetailsViewDeletedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A database error has occured.<br/><br/>" + e.Exception.Message;
            e.ExceptionHandled = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblError.Text = "Another user may have updated that category. <br/><br/>" + "Please try again.";
        }
        else
        {
            this.gvCustomerMaintenance.DataBind();
        }
    }

    /// <summary>
    /// Handles the ItemUpdated event of the dvCustomerMaintenance control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="DetailsViewUpdatedEventArgs" /> instance containing the event data.</param>
    protected void dvCustomerMaintenance_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A database error has occured.<br/><br/>" + e.Exception.Message;
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblError.Text = "Another user may have updated that category. <br/><br/>" + "Please try again.";
        }
        else
        {
            this.gvCustomerMaintenance.DataBind();
        }
    }
}