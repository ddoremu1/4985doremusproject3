﻿using System;
using System.Web.UI;

/// <summary>
///     Code behind contact list page
/// </summary>
/// <version>
///     Spring 2015
/// </version>
/// <author>
///     Danl Doremus
/// </author>
public partial class ContactList : Page
{
    /// <summary>
    ///     The _customer list
    /// </summary>
    private CustomerList _customerList;

    /// <summary>
    ///     Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this._customerList = CustomerList.GetCustomers();

        if (!IsPostBack)
        {
            this.DisplayList();
        }
    }

    /// <summary>
    ///     Displays the list.
    /// </summary>
    private void DisplayList()
    {
        this.lstContactList.Items.Clear();

        this._customerList.Sort();
        for (var i = 0; i < this._customerList.Count(); i++)
        {
            this.lstContactList.Items.Add(this._customerList[i].Display());
        }
    }

    /// <summary>
    ///     Handles the Click event of the btnSelectAdditionalCustomers control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnSelectAdditionalCustomers_Click(object sender, EventArgs e)
    {
        Response.Redirect("Customers.aspx");
    }

    /// <summary>
    ///     Handles the Click event of the btnRemoveCustomer control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnRemoveCustomer_Click(object sender, EventArgs e)
    {
        if (this._customerList.Count() <= 0)
        {
            return;
        }
        if (this.lstContactList.SelectedIndex > -1)
        {
            this._customerList.RemoveAt(this.lstContactList.SelectedIndex);
            this.DisplayList();
        }
        else
        {
            this.lblMessage.Text = "Please select an item to remove.";
        }
    }

    /// <summary>
    ///     Handles the Click event of the btnClearList control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void btnClearList_Click(object sender, EventArgs e)
    {
        if (this._customerList.Count() > 0)
        {
            this._customerList.Clear();
            this.lstContactList.Items.Clear();
        }
    }
}