﻿#define TRACE

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///     This class is used to store the details about the customers in the contact list.
/// </summary>
/// <author>
///     Danl Doremus
/// </author>
/// <version>
///     Spring 2015
/// </version>
public class CustomerList
{
    /// <summary>
    ///     Gets or sets the <see cref="CustomerListItem" /> at the specified index.
    /// </summary>
    /// <value>
    ///     The <see cref="CustomerListItem" />.
    /// </value>
    /// <param name="index">The index.</param>
    /// <returns></returns>
    public CustomerListItem this[int index]
    {
        get { return this._customerList[index]; }
        set { this._customerList[index] = value; }
    }

    /// <summary>
    ///     Gets the <see cref="CustomerListItem" /> with the specified name.
    /// </summary>
    /// <value>
    ///     The <see cref="CustomerListItem" />.
    /// </value>
    /// <param name="name">The name.</param>
    /// <returns></returns>
    public CustomerListItem this[String name]
    {
        get { return this._customerList.FirstOrDefault(c => c.Customer.Name == name); }
    }

    /// <summary>
    ///     The _customer list
    /// </summary>
    private readonly List<CustomerListItem> _customerList;

    /// <summary>
    ///     Initializes a new instance of the <see cref="CustomerList" /> class.
    /// </summary>
    public CustomerList()
    {
        this._customerList = new List<CustomerListItem>();
    }

    /// <summary>
    ///     Counts this instance.
    /// </summary>
    /// <returns></returns>
    public int Count()
    {
        return this._customerList.Count;
    }

    /// <summary>
    ///     Adds the item.
    /// </summary>
    /// <param name="newCustomer">The new customer.</param>
    public void AddItem(Customer newCustomer)
    {
        var customer = new CustomerListItem(newCustomer);
        this._customerList.Add(customer);
    }

    /// <summary>
    ///     Removes at.
    /// </summary>
    /// <param name="index">The index.</param>
    public void RemoveAt(int index)
    {
        this._customerList.RemoveAt(index);
    }

    /// <summary>
    ///     Clears this instance.
    /// </summary>
    public void Clear()
    {
        this._customerList.Clear();
    }

    /// <summary>
    ///     Gets the customers.
    /// </summary>
    /// <returns></returns>
    public static CustomerList GetCustomers()
    {
        var customerList = (CustomerList) HttpContext.Current.Session["CustomerList"];
        if (customerList == null)
        {
            HttpContext.Current.Session["CustomerList"] = new CustomerList();
        }

        return (CustomerList) HttpContext.Current.Session["CustomerList"];
    }

    /// <summary>
    ///     Sorts this instance.
    /// </summary>
    public void Sort()
    {
        this._customerList.Sort(new CompareLastName());
    }
}