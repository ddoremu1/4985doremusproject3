﻿#define TRACE
using System.Diagnostics;

/// <summary>
///     Represents a customer that has been added to the customer list.
/// </summary>
/// <author>
///     Danl Doremus
/// </author>
/// <version>
///     Spring 2015
/// </version>
public class CustomerListItem
{
    /// <summary>
    ///     Gets or sets the customer.
    /// </summary>
    /// <value>
    ///     The customer.
    /// </value>
    public Customer Customer
    {
        get
        {
            Trace.Assert(this._customer != null, "Invalid value");
            return this._customer;
        }
        set { this._customer = value; }
    }

    /// <summary>
    ///     The _customer
    /// </summary>
    private Customer _customer;

    /// <summary>
    ///     Initializes a new instance of the <see cref="CustomerListItem" /> class.
    /// </summary>
    /// <param name="customer">The customer.</param>
    public CustomerListItem(Customer customer)
    {
        this.Customer = customer;
    }

    /// <summary>
    ///     Displays the customer string.
    /// </summary>
    /// <returns></returns>
    public string Display()
    {
        var customerName = this.Customer.Name.Split(' ');
        var firstName = customerName[0];
        var lastName = customerName[1];
        var displayString =
            string.Format(firstName + ", " + lastName + ": " + this.Customer.Phone + "; " + this.Customer.Email);
        return displayString;
    }
}