﻿using System;
using System.Collections.Generic;

/// <summary>
///     Summary description for Compare
/// </summary>
/// <version>
///     Spring 2015
/// </version>
/// <author>
///     Danl Doremus
/// </author>
public class CompareLastName : IComparer<CustomerListItem>
{
    /// <summary>
    ///     Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
    /// </summary>
    /// <param name="x">The first object to compare.</param>
    /// <param name="y">The second object to compare.</param>
    /// <returns>
    ///     A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in
    ///     the following table.Value Meaning Less than zero<paramref name="x" /> is less than <paramref name="y" />.Zero
    ///     <paramref name="x" /> equals <paramref name="y" />.Greater than zero<paramref name="x" /> is greater than
    ///     <paramref name="y" />.
    /// </returns>
    public int Compare(CustomerListItem x, CustomerListItem y)
    {
        var xSplit = x.Customer.Name.Split();
        var xLastName = xSplit[1];

        var ySplit = y.Customer.Name.Split();
        var yLastName = ySplit[1];

        return String.CompareOrdinal(xLastName, yLastName);
    }
}