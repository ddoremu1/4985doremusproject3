﻿using System;
using System.Collections;
using System.Data;
using System.Data.OleDb;

/// <summary>
///     Summary description for FeedbackDatabase
/// </summary>
/// <author>
///     Danl Doremus
/// </author>
/// <version>
///     Spring 2015
/// </version>
public class FeedbackDatabase
{
    /// <summary>
    ///     Gets the open feedback incidents.
    /// </summary>
    /// <param name="supportStaffId">The support staff identifier.</param>
    /// <returns></returns>
    public static IEnumerable GetOpenFeedbackIncidents(int supportStaffId)
    {
        var connection = new OleDbConnection(BallgameDatabase.GetConnectionString());
        const string @select =
            "SELECT Name, SupportID, DateOpened FROM Feedback INNER JOIN Customer ON Customer.CustomerID = Feedback.CustomerID " +
            "WHERE DateClosed IS NULL " + "AND supportId = @supportId ORDER BY DateOpened";
        var command = new OleDbCommand(select, connection);
        command.Parameters.AddWithValue("SupportID", supportStaffId);

        connection.Open();
        var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
        return dataReader;
    }

    /// <summary>
    ///     Gets the customer feedback.
    /// </summary>
    /// <param name="customerId">The customer identifier.</param>
    /// <returns></returns>
    public static IEnumerable GetCustomerFeedback(int customerId)
    {
        var connection = new OleDbConnection(BallgameDatabase.GetConnectionString());
        const string @select =
            "SELECT SupportID, SoftwareID, DateOpened, DateClosed, Title, Description FROM Feedback " +
            "WHERE customerId = @customerId ORDER BY SupportID";
        var command = new OleDbCommand(select, connection);
        command.Parameters.AddWithValue("CustomerID", customerId);

        connection.Open();
        var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
        return dataReader;
    }

    /// <summary>
    ///     Updates the feedback.
    /// </summary>
    /// <param name="originalFeedback">The original feedback.</param>
    /// <param name="newFeedback">The new feedback.</param>
    /// <returns></returns>
    public static int UpdateFeedback(Feedback originalFeedback, Feedback newFeedback)
    {
        var connection = new OleDbConnection(BallgameDatabase.GetConnectionString());
        const string @select =
            "UPDATE Feedback " + "SET DateClosed = @DateClosed, Description = @Description " +
            "WHERE Description = @original_Description";
        var command = new OleDbCommand(select, connection);

        if (Convert.ToDateTime(newFeedback.DateClosed) == Convert.ToDateTime("01/01/0001 12:00:00 AM"))
        {
            command.Parameters.AddWithValue("DateClosed", DBNull.Value);
        }
        else
        {
            command.Parameters.AddWithValue("DateClosed", newFeedback.DateClosed);
        }


        command.Parameters.AddWithValue("Description", newFeedback.Description);


        command.Parameters.AddWithValue("original_Description", originalFeedback.Description);


        connection.Open();
        var updateCount = command.ExecuteNonQuery();
        connection.Close();
        return updateCount;
    }
}