﻿using System;

/// <summary>
///     Summary description for SoftwareObject
/// </summary>
/// <version>
///     Spring 2015
/// </version>
/// <author>
///     Dakotah Doremus
/// </author>
public class SoftwareObject
{
    /// <summary>
    ///     Gets or sets the release date time.
    /// </summary>
    /// <value>
    ///     The release date time.
    /// </value>
    public DateTime ReleaseDate
    {
        get { return this._releaseDate; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._releaseDate = value;
        }
    }

    /// <summary>
    ///     Gets or sets the software version.
    /// </summary>
    /// <value>
    ///     The software version.
    /// </value>
    public double Version
    {
        get { return this._version; }
        set
        {
            if (value < 0)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._version = value;
        }
    }

    /// <summary>
    ///     Gets or sets the name of the software.
    /// </summary>
    /// <value>
    ///     The name of the software.
    /// </value>
    public string Name
    {
        get { return this._name; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._name = value;
        }
    }

    /// <summary>
    ///     Gets or sets the software identifier.
    /// </summary>
    /// <value>
    ///     The software identifier.
    /// </value>
    public string SoftwareId
    {
        get { return this._softwareId; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._softwareId = value;
        }
    }

    /// <summary>
    ///     The _software name
    /// </summary>
    private string _name;

    /// <summary>
    ///     The _release date time
    /// </summary>
    private DateTime _releaseDate;

    /// <summary>
    ///     The _software identifier
    /// </summary>
    private string _softwareId;

    /// <summary>
    ///     The _software version
    /// </summary>
    private double _version;
}