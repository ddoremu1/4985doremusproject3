﻿#define TRACE

using System.Diagnostics;

/// <summary>
///     Stores the details enetered by the user when completing the feedback page.
/// </summary>
/// <author>
///     Danl Doremus
/// </author>
/// <version>
///     Spring 2015
/// </version>
public class Description
{
    /// <summary>
    ///     Gets or sets the contact method.
    /// </summary>
    /// <value>
    ///     The contact method.
    /// </value>
    public int ContactMethod
    {
        get
        {
            Trace.Assert(this._contactMethod > -1, "Invalid value");
            return this._contactMethod;
        }
        set { this._contactMethod = value; }
    }

    /// <summary>
    ///     Gets or sets a value indicating whether this <see cref="Description" /> is contact.
    /// </summary>
    /// <value>
    ///     <c>true</c> if contact; otherwise, <c>false</c>.
    /// </value>
    public bool Contact { get; set; }

    /// <summary>
    ///     Gets or sets the comments.
    /// </summary>
    /// <value>
    ///     The comments.
    /// </value>
    public string Comments
    {
        get
        {
            Trace.Assert(this._comments != null, "Invalid value");
            return this._comments;
        }
        set { this._comments = value; }
    }

    /// <summary>
    ///     Gets or sets the resolution.
    /// </summary>
    /// <value>
    ///     The resolution.
    /// </value>
    public int Resolution
    {
        get
        {
            Trace.Assert(this._resolution > -1, "Invalid value");
            return this._resolution;
        }
        set { this._resolution = value; }
    }

    /// <summary>
    ///     Gets or sets the efficiency.
    /// </summary>
    /// <value>
    ///     The efficiency.
    /// </value>
    public int Efficiency
    {
        get
        {
            Trace.Assert(this._efficiency > -1, "Invalid value");
            return this._efficiency;
        }
        set { this._efficiency = value; }
    }

    /// <summary>
    ///     Gets or sets the service time.
    /// </summary>
    /// <value>
    ///     The service time.
    /// </value>
    public int ServiceTime
    {
        get
        {
            Trace.Assert(this._serviceTime > -1, "Invalid value");
            return this._serviceTime;
        }
        set { this._serviceTime = value; }
    }

    /// <summary>
    ///     Gets or sets the feedback identifier.
    /// </summary>
    /// <value>
    ///     The feedback identifier.
    /// </value>
    public string FeedbackId
    {
        get
        {
            Trace.Assert(this._feedbackId != null, "Invalid value");
            return this._feedbackId;
        }
        set { this._feedbackId = value; }
    }

    /// <summary>
    ///     Gets or sets the customer identifier.
    /// </summary>
    /// <value>
    ///     The customer identifier.
    /// </value>
    public string CustomerId
    {
        get
        {
            Trace.Assert(this._customerId != null, "Invalid value");
            return this._customerId;
        }
        set { this._customerId = value; }
    }

    /// <summary>
    ///     The _comments
    /// </summary>
    private string _comments;

    /// <summary>
    ///     The _contact method
    /// </summary>
    private int _contactMethod;

    /// <summary>
    ///     The _customer identifier
    /// </summary>
    private string _customerId;

    /// <summary>
    ///     The _efficiency
    /// </summary>
    private int _efficiency;

    /// <summary>
    ///     The _feedback identifier
    /// </summary>
    private string _feedbackId;

    /// <summary>
    ///     The _resolution
    /// </summary>
    private int _resolution;

    /// <summary>
    ///     The _service time
    /// </summary>
    private int _serviceTime;
}