﻿using System.Collections;
using System.Data;
using System.Data.OleDb;

/// <summary>
/// Summary description for CustomerDatabase
/// </summary>
/// <author>
/// Danl Doremus
/// </author>
/// <version>
/// Spring 2015
/// </version>
public class CustomerDatabase
{
    /// <summary>
    /// Gets the customers with feedback.
    /// </summary>
    /// <returns></returns>
    public static IEnumerable GetCustomersWithFeedback()
    {
        var connection = new OleDbConnection(BallgameDatabase.GetConnectionString());
        const string @select =
            "SELECT CustomerID, Name " +
            "FROM Customer WHERE CustomerID IN (SELECT DISTINCT CustomerID FROM Feedback WHERE SupportID IS NOT NULL) " +
            " ORDER BY Name";
        var command = new OleDbCommand(select, connection);
        connection.Open();
        var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
        return dataReader;
    }
}