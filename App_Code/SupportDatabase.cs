﻿using System.Collections;
using System.Data;
using System.Data.OleDb;

/// <summary>
///     Summary description for SupportDatabase
/// </summary>
/// <version>
///     Spring 2015
/// </version>
/// <author>
///     Danl Doremus
/// </author>
public class SupportDatabase
{
    /// <summary>
    ///     Gets all support staff.
    /// </summary>
    /// <returns></returns>
    public static IEnumerable GetAllSupportStaff()
    {
        var connection = new OleDbConnection(BallgameDatabase.GetConnectionString());
        const string @select = "SELECT SupportID, Name, Email, Phone " + "FROM Support ORDER BY Name";
        var command = new OleDbCommand(select, connection);
        connection.Open();
        var dataReader = command.ExecuteReader(CommandBehavior.CloseConnection);
        return dataReader;
    }
}