﻿using System;

/// <summary>
/// This class will be used each time a new customer is selected
/// </summary>
/// <author>
/// Danl Doremus
/// </author>
/// <version>
/// Spring 2015
/// </version>
public class Customer
{
    /// <summary>
    /// Gets or sets the email.
    /// </summary>
    /// <value>
    /// The email.
    /// </value>
    /// <exception cref="System.ArgumentException">Invalid data.</exception>
    public string Email
    {
        get { return this._email; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._email = value;
        }
    }

    /// <summary>
    /// Gets or sets the phone.
    /// </summary>
    /// <value>
    /// The phone.
    /// </value>
    /// <exception cref="System.ArgumentException">Invalid data.</exception>
    public string Phone
    {
        get { return this._phone; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._phone = value;
        }
    }

    /// <summary>
    /// Gets or sets the zip code.
    /// </summary>
    /// <value>
    /// The zip code.
    /// </value>
    /// <exception cref="System.ArgumentException">Invalid data.</exception>
    public string ZipCode
    {
        get { return this._zipCode; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._zipCode = value;
        }
    }

    /// <summary>
    /// Gets or sets the state.
    /// </summary>
    /// <value>
    /// The state.
    /// </value>
    /// <exception cref="System.ArgumentException">Invalid data.</exception>
    public string State
    {
        get { return this._state; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._state = value;
        }
    }

    /// <summary>
    /// Gets or sets the city.
    /// </summary>
    /// <value>
    /// The city.
    /// </value>
    /// <exception cref="System.ArgumentException">Invalid data.</exception>
    public string City
    {
        get { return this._city; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._city = value;
        }
    }

    /// <summary>
    /// Gets or sets the address.
    /// </summary>
    /// <value>
    /// The address.
    /// </value>
    /// <exception cref="System.ArgumentException">Invalid data.</exception>
    public string Address
    {
        get { return this._address; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._address = value;
        }
    }

    /// <summary>
    /// Gets or sets the name.
    /// </summary>
    /// <value>
    /// The name.
    /// </value>
    /// <exception cref="System.ArgumentException">Invalid data.</exception>
    public string Name
    {
        get { return this._name; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._name = value;
        }
    }

    /// <summary>
    /// Gets or sets the customer identifier.
    /// </summary>
    /// <value>
    /// The customer identifier.
    /// </value>
    /// <exception cref="System.ArgumentException">Invalid data.</exception>
    public string CustomerId
    {
        get { return this._customerId; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._customerId = value;
        }
    }

    /// <summary>
    /// The _address
    /// </summary>
    private string _address;

    /// <summary>
    /// The _city
    /// </summary>
    private string _city;

    /// <summary>
    /// The _customer identifier
    /// </summary>
    private string _customerId;

    /// <summary>
    /// The _email
    /// </summary>
    private string _email;

    /// <summary>
    /// The _name
    /// </summary>
    private string _name;

    /// <summary>
    /// The _phone
    /// </summary>
    private string _phone;

    /// <summary>
    /// The _state
    /// </summary>
    private string _state;

    /// <summary>
    /// The _zip code
    /// </summary>
    private string _zipCode;
}