﻿using System.Configuration;

/// <summary>
///     Summary description for BallgameDatabase
/// </summary>
/// <version>
///     Spring 2015
/// </version>
/// <author>
///     Danl Doremus
/// </author>
public class BallgameDatabase
{
    /// <summary>
    ///     Gets the connection string.
    /// </summary>
    /// <returns></returns>
    public static string GetConnectionString()
    {
        return ConfigurationManager.ConnectionStrings["DigitalManagerConnectionString"].ConnectionString;
    }
}