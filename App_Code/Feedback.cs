﻿using System;

/// <summary>
///     Represents the feedback entered on the feedback page.
/// </summary>
/// <author>
///     Danl Doremus
/// </author>
/// <version>
///     Spring 2015
/// </version>
public class Feedback
{
    /// <summary>
    ///     Gets or sets the description.
    /// </summary>
    /// <value>
    ///     The description.
    /// </value>
    public string Description
    {
        get { return this._description; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._description = value;
        }
    }

    /// <summary>
    ///     Gets or sets the title.
    /// </summary>
    /// <value>
    ///     The title.
    /// </value>
    public string Title
    {
        get { return this._title; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._title = value;
        }
    }

    /// <summary>
    ///     Gets or sets the date closed.
    /// </summary>
    /// <value>
    ///     The date closed.
    /// </value>
    public string DateClosed { get; set; }

    /// <summary>
    ///     Gets or sets the date opened.
    /// </summary>
    /// <value>
    ///     The date opened.
    /// </value>
    public string DateOpened
    {
        get { return this._dateOpened; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._dateOpened = value;
        }
    }

    /// <summary>
    ///     Gets or sets the support identifier.
    /// </summary>
    /// <value>
    ///     The support identifier.
    /// </value>
    public string SupportId
    {
        get { return this._supportId; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._supportId = value;
        }
    }

    /// <summary>
    ///     Gets or sets the software identifier.
    /// </summary>
    /// <value>
    ///     The software identifier.
    /// </value>
    public string SoftwareId
    {
        get { return this._softwareId; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._softwareId = value;
        }
    }

    /// <summary>
    ///     Gets or sets the customer identifier.
    /// </summary>
    /// <value>
    ///     The customer identifier.
    /// </value>
    public string CustomerId
    {
        get { return this._customerId; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._customerId = value;
        }
    }

    /// <summary>
    ///     Gets or sets the feedback identifier.
    /// </summary>
    /// <value>
    ///     The feedback identifier.
    /// </value>
    public string FeedbackId
    {
        get { return this._feedbackId; }
        set
        {
            if (value == null)
            {
                throw new ArgumentException("Invalid data.");
            }
            this._feedbackId = value;
        }
    }

    /// <summary>
    ///     The _customer identifier
    /// </summary>
    private string _customerId;

    /// <summary>
    ///     The _date opened
    /// </summary>
    private string _dateOpened;

    /// <summary>
    ///     The _description
    /// </summary>
    private string _description;

    /// <summary>
    ///     The _feedback identifier
    /// </summary>
    private string _feedbackId;

    /// <summary>
    ///     The _software identifier
    /// </summary>
    private string _softwareId;

    /// <summary>
    ///     The _support identifier
    /// </summary>
    private string _supportId;

    /// <summary>
    ///     The _title
    /// </summary>
    private string _title;

    /// <summary>
    ///     Formats the feedback.
    /// </summary>
    /// <returns>
    ///     The formatted string
    /// </returns>
    public string FormatFeedback()
    {
        var formattedFeedback = "Feedback for software " + this.SoftwareId + " closed " + this.DateClosed + " (" +
                                this.Description + ")";
        return formattedFeedback;
    }
}