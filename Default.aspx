﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" Runat="Server">
    <link href="Styles/Home.css" rel="stylesheet" type="text/css"/>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="DUMBPlaceHolder" Runat="Server">
    <div class="welcome">
        <asp:Label ID="lblWelcome" runat="server" Text="Welcome to Digital Upscale Manager for Ballgames! We are a company that produces software that helps manage sporting activities. Over the past few years our business has grown exponentially and we are now selling our software all over the country. We have a long list of customers and we strive to meet all of their expectations. Please use this site to give our company feedback and room to improve!"></asp:Label>
        <br/>
        <asp:Button ID="btnCustomerFeedback" runat="server" Text="Complete Customer Feedback" OnClick="btnCustomerFeedback_Click" CssClass="navigationButtons"/><asp:Button ID="btnDisplayCustomers" runat="server" Text="Display Customers" OnClick="btnDisplayCustomers_Click" CssClass="navigationButtons"/>
    </div>
</asp:Content>