﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UpdateFeedback.aspx.cs" Inherits="UpdateFeedback" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="DUMBPlaceHolder" Runat="Server">
    <asp:DropDownList ID="ddlUpdateFeedback" runat="server" DataSourceID="odsUpdateFeedbackDropDown" DataTextField="Name" DataValueField="CustomerID" AutoPostBack="True"></asp:DropDownList>

    <asp:ObjectDataSource ID="odsUpdateFeedbackDropDown" runat="server" SelectMethod="GetCustomersWithFeedback" TypeName="CustomerDatabase"></asp:ObjectDataSource>

    <br/>

    <br/>
    <asp:GridView ID="gvUpdateFeedback" runat="server" AutoGenerateColumns="False" DataSourceID="odsUpdateFeedbackGridView" OnRowUpdated="gvUpdateFeedback_RowUpdated" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White"/>
        <Columns>
            <asp:BoundField DataField="SupportId" HeaderText="Support ID" ReadOnly="True"/>
            <asp:BoundField DataField="SoftwareId" HeaderText="Software ID" ReadOnly="True"/>
            <asp:BoundField DataField="DateOpened" HeaderText="Date Opened" ReadOnly="True"/>
            <asp:BoundField DataField="DateClosed" HeaderText="Date Closed" ReadOnly="False" NullDisplayText=""/>
            <asp:BoundField DataField="Title" HeaderText="Feedback Title" ReadOnly="True"/>
            <asp:TemplateField HeaderText="Description">
                <EditItemTemplate>
                    <asp:TextBox ID="txtEditDescription" runat="server" Text='<%#
    Bind("Description") %>' TextMode="MultiLine">
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEditDescription" runat="server" ControlToValidate="txtEditDescription" Display="Dynamic" ErrorMessage="This is a required field.">*</asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%#
    Bind("Description") %>'>
                    </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowEditButton="True"/>
        </Columns>
        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White"/>
        <HeaderStyle BackColor="#8ABC00" Font-Bold="True" ForeColor="White"/>
        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center"/>
        <RowStyle BackColor="lightgrey" ForeColor="#333333"/>
        <SelectedRowStyle BackColor="#BDC58F" Font-Bold="True" ForeColor="Navy"/>
        <SortedAscendingCellStyle BackColor="#FDF5AC"/>
        <SortedAscendingHeaderStyle BackColor="#4D0000"/>
        <SortedDescendingCellStyle BackColor="#FCF6C0"/>
        <SortedDescendingHeaderStyle BackColor="#820000"/>
    </asp:GridView>

    <asp:ObjectDataSource ID="odsUpdateFeedbackGridView"
                          runat="server"
                          SelectMethod="GetCustomerFeedback"
                          TypeName="FeedbackDatabase"
                          ConflictDetection="CompareAllValues"
                          OldValuesParameterFormatString="originalFeedback"
                          DataObjectTypeName="Feedback"
                          UpdateMethod="UpdateFeedback">
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlUpdateFeedback" Name="customerId" PropertyName="SelectedValue" Type="Int32"/>
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="originalFeedback" Type="Object"/>
            <asp:Parameter Name="newFeedback" Type="Object"/>
        </UpdateParameters>
    </asp:ObjectDataSource>

    <asp:ValidationSummary ID="vsUpdateFeedback" runat="server" HeaderText="Please fix the following errors:"/>

    <br/>
    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>

</asp:Content>