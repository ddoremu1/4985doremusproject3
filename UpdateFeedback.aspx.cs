﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Code behind for UpdateFeedback page
/// </summary>
/// <author>
/// Danl Doremus
/// </author>
/// <version>
/// Spring 2015
/// </version>
public partial class UpdateFeedback : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Handles the RowUpdated event of the gvUpdateFeedback control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void gvUpdateFeedback_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A database error has occurred.<br /><br />" +
                                 e.Exception.Message;
            if (e.Exception.InnerException != null)
            {
                this.lblError.Text += "<br />Message: "
                                      + e.Exception.InnerException.Message;
            }
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblError.Text = "Another user may have updated that category."
                                 + "<br />Please try again.";
        }
    }
}